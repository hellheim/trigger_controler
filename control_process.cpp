#include "header/control_process.hpp"

extern int vid_cap_fd;
extern int face_rec_fd;
extern int blinking_delay;
extern bool process_ongoing;

void control_process(int fd, std::vector<process_state> *process_states, bool *siglistner, int ind ,bool *rdy_ctrl)
{
    char buffer[12];
    int n;
    bool_tr send = notsend;
    int bpid = 0;
    process_state state;
    //std::cout << "my file descriptor is: " << fd << std::endl;
    bzero(buffer, sizeof(buffer));
    strcpy(buffer,SEND_STREAM);
    n = write(fd, buffer, sizeof(buffer));
    if(n < 0)
        error_("worker: cannot read from socket..");
    bzero(buffer, sizeof(buffer));
    n = read(fd, buffer,sizeof(buffer));
    if(n < 0)
        error_("worker: cannot read from socket..");
    bpid = std::atoi(buffer);
    rdy_ctrl[ind] = true;
    //std::cout << "*" << bpid << "*" << std::endl;
    if(bpid == VideoCapture)
    {
        vid_cap_fd = fd;
    }
    switch(bpid)
    {
        case VideoCapture:
            state.bpid = VideoCapture;
            state.log_state = (char *)malloc(sizeof(char)*strlen("VideoCapture: working.."));
            strcpy(state.log_state,"VideoCapture: working..");
            state.code_state = 0;
            process_states->push_back(state);
            do
            {
            }while(*siglistner == false);
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,EXIT);
            n = write(fd,buffer,sizeof(buffer));
            if(n < 0)
                std::cout << "Unable to write to socket" << std::endl;
            std::cout << "Stopping.." << std::endl;
            break;
        case GetConfigUser:
            state.bpid = GetConfigUser;
            state.log_state = (char *)malloc(sizeof(char)*strlen("GetConfigUser: working.."));
            strcpy(state.log_state,"GetConfigUser: working..");
            state.code_state = 0;
            process_states->push_back(state);
            do
            {
            }while(*siglistner == false);
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,EXIT);
            n = write(fd,buffer,sizeof(buffer));
            if(n < 0)
                std::cout << "Unable to write to socket" << std::endl;
            std::cout << "Stopping.." << std::endl;
            break;
        case FaceDetector:
            state.bpid = FaceDetector;
            state.log_state = (char *)malloc(sizeof(char)*strlen("FaceDetector: working.."));
            strcpy(state.log_state,"FaceDetector: working..");
            state.code_state = 0;
            process_states->push_back(state);
            do
            {
            }while(*siglistner == false);
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,EXIT);
            n = write(fd,buffer,sizeof(buffer));
            if(n < 0)
                std::cout << "Unable to write to socket" << std::endl;
            std::cout << "Stopping.." << std::endl;
            break;
        case FaceRecognizer:
            state.bpid = FaceDetector;
            state.log_state = (char *)malloc(sizeof(char)*strlen("FaceRecognizer: working.."));
            strcpy(state.log_state,"FaceRecognizer: working..");
            state.code_state = 0;
            process_states->push_back(state);
            do
            {
                bzero(buffer,sizeof(buffer));
                n = read(fd,buffer,sizeof(buffer));
                if(strcmp(buffer,ASK) == 0)
                {
                    std::cout << "Average under threshold, considered as other user, add it?" << std::endl;
                    face_rec_fd = fd;
                }
                else if(strcmp(buffer, EXIT) == 0)
                    *siglistner = true;
                else if(strcmp(buffer,"finished") == 0)
                {
                    printf("FaceRecognizer: %s",buffer);
                    process_ongoing = false;
                    blinking_delay = 500;
                }
            }while(*siglistner == false);
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,EXIT);
            n = write(fd,buffer,sizeof(buffer));
            if(n < 0)
                std::cout << "Unable to write to socket" << std::endl;
            std::cout << "Stopping.." << std::endl;
            break;
        default:
            std::cout << "Stopping.." << std::endl;
            break;
    }
}