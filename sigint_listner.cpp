#include "header/sigint_listner.hpp"

void sigint_listner(int fd, std::vector<process_state> *process_states, int ind , bool *siglistner, bool *rdy_ctrl)
{
    char buffer[12];
    int n;
    do
    {
        if(rdy_ctrl[ind])
        {
            bzero(buffer, sizeof(buffer));
            n = read(fd, buffer, sizeof(buffer));
            if(n < 0)
                error_("SIGINT_LISTNER: Can't read from socket");
        }
    }while(strcmp(buffer,EXIT) != 0);
    process_states->at(ind).code_state = 1;
    switch(process_states->at(ind).bpid)
    {
        case VideoCapture:
            strcpy(process_states->at(ind).log_state,"VideoCapture: stopped..");
            break;
        case FaceDetector:
            strcpy(process_states->at(ind).log_state,"FaceDetector: stopped..");
            break;
        case FaceRecognizer:
            strcpy(process_states->at(ind).log_state,"FaceRecognizer: stopped..");
            break;
        case GetConfigUser:
            strcpy(process_states->at(ind).log_state,"GetConfigUser: stopped..");
            break;
    }
    *siglistner = true;
    std::cout << "Closing program.." << std::endl;
    exit(0);
}