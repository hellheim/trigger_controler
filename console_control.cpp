#include "header/console_control.hpp"

extern int vid_cap_fd;
extern int face_rec_fd;
extern int blinking_delay;

void console_control(std::vector<process_state> *process_states, bool *siglistner)
{
    char buffer[12];
    bool_tr send;
    int n;
    std::cout << "write: help to get usable command" << std::endl;
    do
    {
        bzero(buffer,sizeof(buffer));
        std::cout << ">";
        std::cin >> buffer;
        send = prompt_interpreter(buffer,*process_states);
        if(send == bool_tr::sends)
        {
            if(vid_cap_fd != -125)
            {
                blinking_delay = 200;
                bzero(buffer,sizeof(buffer));
                strcpy(buffer,SEND_STREAM);
                n = write(vid_cap_fd,buffer,sizeof(buffer));
                if(n < 0)
                    std::cout << "Unable to write to socket" << std::endl;
            }
            else
            {
                blinking_delay = 500;
                std::cout << "Process not ready yet." << std::endl;
            }
        }
        else if(send == bool_tr::yes)
        {
            if(face_rec_fd != -125)
            {
                
                bzero(buffer,sizeof(buffer));
                strcpy(buffer,ADD);
                n = write(face_rec_fd,buffer,sizeof(buffer));
                if(n < 0)
                    std::cout << "Unable to write to socket" << std::endl;
                face_rec_fd = -125;
            }
            else
            {
                
                std::cout << "Process not ready yet." << std::endl;
            }
        }
        else if(send == no)
        {
            if(face_rec_fd != -125)
            {
                bzero(buffer,sizeof(buffer));
                strcpy(buffer,"NOADD");
                n = write(face_rec_fd,buffer,sizeof(buffer));
                if(n < 0)
                    std::cout << "Unable to write to socket" << std::endl;
                face_rec_fd = -125;
            }
            else
            {
                std::cout << "Process not ready yet." << std::endl;
            }
        }
        else if( send == stop)
        {
            *siglistner = true;
            digitalWrite(BLINKING_LED,LOW);
            digitalWrite(ON_LED,LOW);
            exit(0);
        }
    }while(*siglistner == false);
}