#include "header/prompt_interpreter.hpp"

bool_tr prompt_interpreter(char *buffer, std::vector<process_state> process_states)
{
    std::string help = " version 1.1\n";
    help += " ____ ____        __  __  ___  ____  _   _ _     _____ \n";
    help += "| __ ) ___|      |  \\/  |/ _ \\|  _ \\| | | | |   | ____|\n";
    help += "|  _ \\___ \\ _____| |\\/| | | | | | | | | | | |   |  _|  \n";
    help += "| |_) |__) |_____| |  | | |_| | |_| | |_| | |___| |___ \n";
    help += "|____/____/      |_|  |_|\\___/|____/ \\___/|_____|_____|\n\n";
    help += "help: shows this help\n";
    help += "send: send a trigger to simulate a driver entrance\n";
    help += "state: display process status\n";
    help += "yes/no: answer adding new user\n";
    help += "exit: close program\n";
    if(strcmp(buffer, "help") == 0)
    {
            std::cout << help;
            return notsend;
    }
    else if(strcmp(buffer, "send") == 0)
    {
        return sends;
    }
    else if(strcmp(buffer, "yes") == 0)
    {
        return yes;
    }
    else if(strcmp(buffer,"no") == 0)
    {
        return no;
    }
    else if(strcmp(buffer, "state") == 0)
    {
        if(!process_states.empty())
        {
            for(size_t i = 0; i < process_states.size(); i++)
            {
                std::cout << "Process: " << process_states.at(i).bpid << std::endl;
                std::cout << "Log: " << process_states.at(i).log_state << std::endl;
                std::cout << "Code log: " << process_states.at(i).code_state << std::endl;
            }
        }
        else
        {
            std::cout << "Process not yet initialized.. " << std::endl;
        }
        return notsend;
    }
    else if(strcmp(buffer, "exit") == 0)
    {
        return stop;
    }
    else
    {
        std::cout << "This function is not implemented" << std::endl;
        return notsend;
    }
    return notsend;
}