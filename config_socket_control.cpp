#include "header/config_socket_control.hpp"

void config_socket_control(config_struct_local *config_, const char* socket_name, int *client_sockfd, std::thread **workers, std::thread **sig_listner, std::vector<process_state> *process_states, bool *siglistner, bool *ready_control)
{
    int i = 0;
    strcpy(config_->socket_name, socket_name);
    config_->sockfd = socket(PF_LOCAL,SOCK_STREAM,0);
    config_->serv_addr.sun_family = AF_LOCAL;
    strcpy(config_->socket_name,socket_name);
    strcpy(config_->serv_addr.sun_path,config_->socket_name);
    socklen_t length_ = sizeof(config_->serv_addr);
    bind(config_->sockfd,(const sockaddr*)&(config_->serv_addr),length_);
    listen(config_->sockfd,NBR_PORCESS);
    while(i < NBR_PORCESS)
    {
        ready_control[i] = false;
        client_sockfd[i] = accept(config_->sockfd,(struct sockaddr*) &(config_->client_addr),&length_);
        if(client_sockfd[i] < 0)
        {
            error_("accept: Unable to accept connections..");
        }
        workers[i] = new std::thread(control_process,client_sockfd[i],process_states, siglistner, i , ready_control);
        //sig_listner[i] = new std::thread(sigint_listner,client_sockfd[i],process_states,i,siglistner, ready_control);
        std::cout << "Accepted connection: " << i << " working thread lunched." << std::endl;
        i++;
    }
}