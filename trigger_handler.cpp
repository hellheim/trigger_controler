#include "header/trigger_handler.hpp"

extern int vid_cap_fd;
extern int blinking_delay;
extern bool process_ongoing;

void trigger_handler(void)
{
    char buffer[12];
    int n;
    if(vid_cap_fd != -125)
    {
        if(!process_ongoing)
        {
            printf("gotcha\n");
            process_ongoing = true;
            blinking_delay = 200;
            bzero(buffer,sizeof(buffer));
            strcpy(buffer,SEND_STREAM);
            n = write(vid_cap_fd,buffer,sizeof(buffer));
            if(n < 0)
                std::cout << "Unable to write to socket" << std::endl;
        }
    }
    else
    {
        blinking_delay = 500;
        std::cout << "Process not ready yet." << std::endl;
    }
}