/**
 * function: error_
 * Description: Cette fonction sers à afficher un message d'erreur personnalisé et
 * à quitter le programme avec un code de retour 1.
 * */


#ifndef ERROR_HPP
#define ERROR_HPP
#include <string>

void error_(std::string msg);

#endif