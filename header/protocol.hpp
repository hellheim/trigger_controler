#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP
#include <string>


#define SEND_STREAM "send_stream"
#define ACCEPT_STREAM "streaming_"
#define STOP_STREAM "stop_stream"
#define EXIT "exit"
#define ASK "ask"
#define ADD "add"

//defining custom pid
#define VideoCapture 0
#define FaceDetector 1
#define FaceRecognizer 2
#define GetConfigUser 3


void protocol(int sockfd, char *MESSAGE, std::string msg);

#endif