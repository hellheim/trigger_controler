/**
 * function: console_control
 * Description: thread invoqué au tout début du programme afin d'offrir la console
 * au développeur pour qu'il puisse monitoré l'état des autres processus, en plus des commandes
 * de controle.
 * */

#ifndef CONSOLE_CONTROL_HPP
#define CONSOLE_CONTROL_HPP

#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <wiringPi.h>

#include "process_state.hpp"
#include "error_.hpp"
#include "protocol.hpp"
#include "prompt_interpreter.hpp"

#ifndef BLINKING_LED
#define BLINKING_LED 9
#define ON_LED 8
#define INTERRUPTION 7
#endif

void console_control(std::vector<process_state> *process_states, bool *siglistner);

#endif