/**
 * struct: process_state
 * Description:cette structure modélise l'état d'un process en cours d'exécution, sauvegardant 
 * son pid( personnalized pid), son historique de log et son code de retour de la dernière exécution.
 * */

#ifndef PROCESS_STATE_HPP
#define PROCESS_STATE_HPP

typedef struct {
    uint8_t bpid;
    char *log_state;
    int code_state;
}process_state;

#endif