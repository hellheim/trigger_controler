#ifndef CONTROL_PROCESS_HPP
#define CONTROL_PROCESS_HPP

#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 

#include "process_state.hpp"
#include "error_.hpp"
#include "protocol.hpp"
#include "prompt_interpreter.hpp"


/**
 * This function is to be called as a worker thread to handle communication between
 * trigger_control process and the others.
 * the function will behave according to BPID
 * */

void control_process(int fd, std::vector<process_state> *process_states, bool *siglistner, int ind ,bool *rdy_ctrl);

#endif