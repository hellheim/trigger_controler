#ifndef SIGINT_LISTNER_HPP
#define SIGINT_LISTNER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 

#include "process_state.hpp"
#include "error_.hpp"
#include "protocol.hpp"
#include "prompt_interpreter.hpp"

/**
 * function: sigint_listner
 * description: Cette fonction sert à être invoqué en tant qu'objet std::thread pour servir
 * de processus léger en écoute d'un signal d'interruption en provenance de chaque processus
 * et mettre à jour la variable siglistner afin de diffuser l'état de cette variable aux autres threads.
 * */

void sigint_listner(int fd, std::vector<process_state> *process_states, int ind ,bool *siglistner, bool *rdy_ctrl);

#endif