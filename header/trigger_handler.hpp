#ifndef TRIGGER_HANDLER_HPP
#define TRIGGER_HANDLER_HPP

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 

#include "protocol.hpp"

void trigger_handler(void);

#endif