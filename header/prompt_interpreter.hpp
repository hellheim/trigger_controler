/**
 * function: prompt_interpreter
 * Description: Cette fonction offre un service de console dynamique au développeur, permettant de controler
 * les processus ou de déclencher la routine de détection. 
 * La structure permet de définir un type booléen amélioré, représentant 3 état.
 * */


#ifndef PROMPT_INTERPRETER_HPP
#define PROMPT_INTERPRETER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <string.h>
#include <stdlib.h>

#include "process_state.hpp"

enum bool_tr {
    sends,
    notsend,
    stop,
    yes,
    no
};

bool_tr prompt_interpreter(char *buffer, std::vector<process_state> process_states);

#endif