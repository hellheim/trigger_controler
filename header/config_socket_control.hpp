#ifndef CONFIG_SOCKET_CONTROL_HPP
#define CONFIG_SOCKET_CONTROL_HPP

#include <iostream>
#include <string>
#include <thread>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "config_struct.hpp"
#include "error_.hpp"
#include "control_process.hpp"
#include "sigint_listner.hpp"

#define NBR_PORCESS 4

//function to configure control_socket
/**
 * Update 1.1: the trigger_controler will from now control every process and in a future release it will be the main process
 * the function will accept N communications, each one to a process
 * */

void config_socket_control(config_struct_local *config_, const char* socket_name, int *client_sockfd, std::thread **workers, std::thread **sig_listner, std::vector<process_state> *process_states, bool *siglistner, bool *ready_control);

#endif