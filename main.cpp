/**
 * Program name: trigger_controler
 * version: 1.1
 * author: Belha
 * Description: Ce processus représente le noeud centrale de tout le module.
 * Il sert à controler et surveiller l'état des autres processus ainsi qu'à déclencher
 * la routine de capture via la commande send (Cette commande pourra être substituer
 * au futur par un capteur physique lié au GPIO de la carte de développement).
 * */


#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <thread>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <wiringPi.h>
#include <errno.h>

#include "header/config_socket_control.hpp"
#include "header/process_state.hpp"
#include "header/protocol.hpp"
#include "header/console_control.hpp"
#include "header/trigger_handler.hpp"

#define BLINKING_LED 9
#define ON_LED 8
#define INTERRUPTION 7

using namespace std;

int vid_cap_fd = -125, face_rec_fd = -125, blinking_delay = 500;
bool process_ongoing = false;

void blinkled(bool *sig, int *blik)
{
    while(*sig == false)
    {
        digitalWrite(BLINKING_LED,HIGH);
        delay(*blik);
        digitalWrite(BLINKING_LED,LOW);
        delay(*blik);
    }
}

int main(int argc, char **argv)
{
    if(argc < 2)
    {
        cout << "Please use like: " << argv[0] << " <control-socket>" << endl;
        exit(1);
    }
    wiringPiSetup();
    pinMode(INTERRUPTION,INPUT);
    pinMode(ON_LED,OUTPUT);
    pinMode(BLINKING_LED,OUTPUT);
    digitalWrite(ON_LED,HIGH);
    wiringPiISR(INTERRUPTION, INT_EDGE_FALLING, &trigger_handler);
    config_struct_local config_;
    int n, client_sockfd[NBR_PORCESS];
    vector<process_state> states;
    bool siglistner = false, ready_control[NBR_PORCESS];
    thread *workers[NBR_PORCESS], *sig_listner[NBR_PORCESS], *console, *blinky;
    char *socket_name = argv[1];
    console = new thread(console_control,&states,&siglistner);
    blinky = new thread(blinkled,&siglistner,&blinking_delay);
    config_socket_control(&config_, socket_name, client_sockfd,workers,sig_listner,&states,&siglistner, ready_control);
    for(int i = 0; i < NBR_PORCESS; i++)
    {
        workers[i]->join();
        //sig_listner[i]->join();
    }
    return 0;
}